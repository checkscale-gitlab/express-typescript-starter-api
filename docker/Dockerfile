###
# build image
###
FROM node:15-alpine3.10 as build
WORKDIR /app

# get dependencies
COPY package.json package.json
COPY yarn.lock yarn.lock
COPY tsconfig.build.json tsconfig.build.json
COPY tsconfig.json tsconfig.json
RUN yarn

# copy all that aren't included in .dockerignore
COPY . .

# build app

EXPOSE 3000

RUN yarn build

###
# dependency image
###
FROM node:15-alpine3.10 as dep

WORKDIR /app
COPY --from=build ./app/package.json package.json
COPY --from=build ./app/yarn.lock yarn.lock

RUN yarn --production


###
# minimal image
###
FROM node:15-alpine3.10

COPY --from=build /app/dist ./dist
COPY --from=dep /app/node_modules ./node_modules
COPY package.json yarn.lock ./
COPY ormconfig.docker.json ./ormconfig.json


ENTRYPOINT yarn start:prod