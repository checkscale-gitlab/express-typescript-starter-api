// From: https://stackoverflow.com/questions/31626231/custom-error-class-in-typescript.
// & https://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-2.html.
//
// Allows for status being sent within a throwable error.
export class HttpError extends Error {
    protected status: number;
    constructor(m: string, StatusCode: number) {
        super(m);
        this.status = StatusCode;

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, HttpError.prototype);
    }
}