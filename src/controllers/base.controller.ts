import express from 'express';

/** Base class to be extended for new controllers.
*/
export abstract class BaseController {
    public path: string;
    public router = express.Router();

    constructor (path: string) 
    {
        this.path = path;
        this.initialiseRoutes();
    }

    public abstract initialiseRoutes(): void;
}